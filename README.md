## Description
Классификация изображений алфавита американского жестового языка (American Sign Language, ASL) с использованием модели глубокого обучения, реализованной в PyTorch.
Имплементация включает в себя интеграцию CI/CD для развертывания модели на рабочий сервер с использованием инструментов Clear ML для отслеживания метрик и настройки автоматического обучения модели после каждого коммита в GitLab. Гиперпараметры модели могут быть настроены через переменные окружения GitLab. 
Модель, используемая в данном проекте, представляет собой сверточную нейронную сеть (Convolutional Neural Network, CNN), реализованную с использованием фреймворка глубокого обучения PyTorch.

## CI/CD Description
1. После коммита, начинает выполнятся CI/CD
2. Все Креды хранятся в GitLab в качестве переменных или секретов
3. Мы используем DVC и GoogleDrive для подгрузки данных
4. После подгрузки данных инициализируется эксперимент clearml
5. Начинает обучаться модель, все метрики логируются в clearml в реальном времени, также clearml сохраняет модель и промежуточные модели
### Эксперимент можно посмотреть:
- [тут](https://app.clear.ml/projects/2b0e4dbf4e1847d5a1cc892d0916f734/experiments/824085a6c5084769ab3c2c59d88cd1d8/output/debugImages)
# Frameworks/Libraries/Tools
 - dvc
 - clearml
 - poetry
 - pytorch

# How to manage with project:
 1. Устанавливаем poetry 
 ``` pip install poetry ```
 2. Устанавливаем зависимости из poetry
 ``` poetry install ```
 3. Скачиваем сырые даные
 ``` cd scripts ```
 ``` sh download_data.sh ```
 4. Инициализируем обучение
 ``` cd scripts ```
 ``` sh start_trainig.sh ```

# Project configuration
```

├── .dvc                      <- DVC dependencies
├── clear_ml                  <- clearml configuration
│   ├── __init__.py     
│   │   
│   ├── config_clearml.py     
│
├── README.md                 <- Current README.md file
├── data                      <- test/train dataset 
│   ├── test                       
│   │   
│   ├── train                 
│
├── models                    <- Model code and configuration
│   │   
│   ├── resnet50
│       │
│       └── model_config.py   
│       │
│       └── utils.py          
├── scripts                   <- Scripts being used for dataset download and trainig stage beginning 
│   │   
│   ├── download_data.sh       <- model's code
│   │   
│   ├── start_trainigs.sh
│
├── secrets                    <- Sensetive data/credentials storage 
│    
├── tests                      <- Pytests storage 
│   │   
│   ├── test.py   
├── pyproject.toml     
├── .gitignore          
├── .gitlab-ci.yml  
├── train_model.py 
└── requirements.txt
```
