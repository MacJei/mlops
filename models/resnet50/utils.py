from abc import ABC
from os import PathLike

import torch
from torch.nn import CrossEntropyLoss, Linear
from torch.optim import Adam
from torch.utils.data import DataLoader, Dataset, Subset
from torchvision import datasets
from torchvision.models import resnet50
from torchvision.transforms import Compose

from clear_ml.config_clearml import ClearML

torch.manual_seed(1)


class DataPrepare(ABC):
    def __init__(
        self,
        train_data_path: PathLike,
        transforms: Compose,
        num_train_samples: int,
        batch_size: int,
        val_split: float,
    ):
        self.train_data_path = train_data_path
        self.transforms = transforms
        self.num_train_samples = int(num_train_samples)
        self.val_split = float(val_split)
        self.batch_size = int(batch_size)

    def dataloader_prepare(self) -> None:
        """
        Метод подготавливает данные для обучения
        """

        train_dataset = datasets.ImageFolder(
            self.train_data_path, 
            transform=self.transforms
        )
        val_dataset = datasets.ImageFolder(
            self.train_data_path, 
            transform=self.transforms
        )

        split = int(self.num_train_samples * self.val_split)
        indices = torch.randperm(self.num_train_samples)

        train_subset = Subset(train_dataset, indices[split:])
        val_subset = Subset(val_dataset, indices[:split])

        self.len_train_subset = len(train_subset)
        self.len_val_subset = len(val_subset)

        self.train_dataloader = DataLoader(
            dataset=train_subset, batch_size=self.batch_size, shuffle=True
        )

        self.val_dataloader = DataLoader(
            dataset=val_subset, batch_size=self.batch_size, shuffle=False
        )
        self.classes = self.train_dataloader.dataset.dataset.classes


class Model(ABC):
    def __init__(
        self,
        DataPrepare: DataPrepare,
        learning_rate: float,
        num_epoche: int,
        device: str,
    ):
        self.DataPrepare = DataPrepare
        self.learning_rate = learning_rate
        self.num_epoche = num_epoche
        self.device = device

        self.criterion = CrossEntropyLoss()
        self.model = resnet50(pretrained=True)
        for param in self.model.parameters():
            param.requires_grad = False

        self.in_features = self.model.fc.in_features
        fc = Linear(
            in_features=self.in_features, 
            out_features=len(self.DataPrepare.classes)
        )
        self.model.fc = fc

        params_to_update = []
        for name, param in self.model.named_parameters():
            if param.requires_grad == True:
                params_to_update.append(param)
        self.optimizer = Adam(
            params_to_update, 
            lr=self.learning_rate
        )


    def train(self) -> None:
        """
        Метод запускает обучение модели
        """
        clearml = ClearML(project_name='mlops', task_name='learning-stage')

        steps = 0
        self.train_losses, self.val_losses = [], []

        self.model.to(self.device)
        
        print("Start...")
        for epoch in range(self.num_epoche):
            print(f"Epoche {epoch}")
            running_loss = 0
            correct_train = 0
            total_train = 0

            self.model.train()
            for i, (images, labels) in enumerate(self.DataPrepare.train_dataloader):
                steps += 1
                images = images.to(self.device)
                labels = labels.to(self.device)

                # Forward pass
                output = self.model(images)
                loss = self.criterion(output, labels)

                correct_train += (torch.max(output, dim=1)[1] == labels).sum()
                total_train += labels.size(0)

                # Backward and optimize
                self.optimizer.zero_grad()
                loss.backward()
                self.optimizer.step()

                running_loss += loss.item()

                # Logging
                clearml.report_scalar('training', 'loss', running_loss / steps, iteration=steps)
                clearml.report_scalar('training', 'accuracy', correct_train / total_train, iteration=steps)

            with torch.no_grad():
                self.model.eval()
                correct_val, total_val = 0, 0
                val_loss = 0
                for images, labels in self.DataPrepare.val_dataloader:
                    images = images.to(self.device)
                    labels = labels.to(self.device)
                    output = self.model(images)
                    loss = self.criterion(output, labels)
                    val_loss += loss.item()

                    correct_val += (torch.max(output, dim=1)[1] == labels).sum()
                    total_val += labels.size(0)

            clearml.report_scalar('validation', 'loss', val_loss / len(self.DataPrepare.val_dataloader), iteration=steps)
            clearml.report_scalar('validation', 'accuracy', correct_val / total_val, iteration=steps)

            self.train_losses.append(running_loss / total_train)
            self.val_losses.append(val_loss / total_val)

            model_pth = 'temp_checkpoint.pth'
            torch.save(self.model.state_dict(), f'temp_checkpoint.pth')
            clearml.task.upload_artifact(name='torch_model', artifact_object=model_pth)
            # перестаем обучаться, если значение нас устраивает
            if correct_val / total_val >= 0.75:
                break
