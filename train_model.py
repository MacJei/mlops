from os import PathLike
import os

import fire
from models.resnet50 import DataPrepare, Model, ModelConfig


def train_model() -> None:
    """
    Функция запускает обучение модели
    """
    data_prepare_cls = DataPrepare(
        ModelConfig.TRAIN_DATA_PATH, 
        ModelConfig.TRANSFORMS, 
        int(os.environ['NUM_TRAINS_SPLIT']), 
        int(os.environ['BATCH_SIZE']),
        float(os.environ['VAL_SPLIT'])
    )
    data_prepare_cls.dataloader_prepare()

    model = Model(
        DataPrepare=data_prepare_cls,
        learning_rate=float(os.environ['OPTIMIZER_LR']),
        num_epoche=int(os.environ['NUM_EPOCHES']),
        device=ModelConfig.DEVICE,
    )
    model.train()


if __name__ == "__main__":
    fire.Fire(train_model)
